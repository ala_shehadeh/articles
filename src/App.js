import React, { Component } from 'react';
import AddArticle from "./components/AddArticle";
import AllArticles from "./components/AllArticles";

class App extends Component {
  render() {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-9 col-sm-12 col-xs-12" id="main">
                    <AllArticles/>
                </div>
                <div id="form" className="col-md-3 col-sm-12 col-xs-12">
                    <AddArticle/>
                </div>
            </div>
        </div>
    );
  }

}

export default App;
