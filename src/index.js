import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import './style.css'
import App from './App';

global.host = 'http://localhost:8080/'

ReactDOM.render(<App />, document.getElementById('root'));