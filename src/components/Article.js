import React,{Component} from 'react'
import axios from 'axios'
import ReactDOM from 'react-dom'
import AllArticles from "./AllArticles";

class Article extends Component {
    articles() {
        ReactDOM.render(<AllArticles/>,document.getElementById('main'))
    }
    constructor(props) {
        super()
        this.state = {
            id: props.id,
            title: null,
            body: null
        }
        this.getArticle = this.getArticle.bind(this)
        this.getArticle()
    }
    getArticle() {
        var $this = this;
        axios.get(global.host+'article/'+this.state.id).then(function (xhr) {
            if(xhr.data) {
                $this.setState({
                    title: xhr.data.title,
                    body: xhr.data.body
                })
            }
        }).catch(function (xhr) {
            if(xhr.response) {
                ReactDOM.render(<div className="alert alert-danger lead text-center">{xhr.response.data.error}</div>
                ,document.getElementById('msg'))
            }
        })
    }
    render() {
        return (
            <div>
            <article>
                <div className="row">
                    <div className="col-md-12"><h1>{this.state.title}</h1></div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-md-12">{this.state.body}</div>
                </div>
            </article>
                <hr />
                <div className="cursor" onClick={this.articles}><i className="fa fa-backward"></i> Return to articles page</div>
            </div>
        )
    }
}

export default Article
