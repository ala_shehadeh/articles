import React,{Component} from 'react'

class ImageLoader extends Component {
    render() {
        return (
            <div className="text-center"><img src={require("../images/loader.gif")}/>
            </div>
        )
    }
}
export default ImageLoader