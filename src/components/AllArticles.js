import React,{Component} from 'react'
import ReactDOM from 'react-dom'
import axios from 'axios'

import 'font-awesome/css/font-awesome.min.css'
import EditArticle from "./EditArticle";
import ImageLoader from "./ImageLoader";
import Article from "./Article";

class AllArticles extends Component {
    editArticle(data) {
        ReactDOM.render(null,document.getElementById('form'),function () {
            ReactDOM.render(<EditArticle data={data}/>,document.getElementById('form'))
        })
    }
    selectedArticle(id) {
        ReactDOM.render(<Article id={id}/>,document.getElementById('main'))
    }
    deleteArticle(id) {
        var $this = this
        axios.delete(global.host + 'article/'+id)
            .then(function () {
                $this.allArticles()
            })
    }
     allArticles() {
        var $this = this
        axios.get(global.host + 'articles')
            .then(function (xhr) {
                $this.setState({
                    articles: xhr.data.data
                })
            })
            .catch(function (xhr) {
                if(xhr.response) {
                    ReactDOM.render(<div className="alert alert-warning lead text-center">
                        No articles at the database
                    </div>,document.getElementById('main'))
                }
            })
    }
    constructor() {
        super()
        this.state = {
            articles: []
        }
        this.allArticles = this.allArticles.bind(this)
        this.deleteArticle = this.deleteArticle.bind(this)
        this.allArticles()
    }
    render() {
        return (
            <div>
                <ul className="list-group">
                    {
                        this.state.articles.map((value,key)=>
                         <li key={key} className="list-group-item">
                         <div className="icon"><i className="fa fa-edit" onClick={()=>this.editArticle(value)}></i></div>
                         <div className="icon"><i className="fa fa-trash" onClick={()=>this.deleteArticle(value.id)}></i></div>
                             <div className="cursor" onClick={()=>this.selectedArticle(value.id)}> {value.title}</div>
                         </li>
                        )
                    }
                </ul>
            </div>
        )
    }
}
export default AllArticles