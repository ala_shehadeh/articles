import React,{Component} from 'react'
import ImageLoader from "./ImageLoader";
import ReactDOM from 'react-dom'
import axios from 'axios'
import AllArticles from "./AllArticles";

class AddArticle extends Component {
    addArticle(event) {
        var $this = this
        ReactDOM.render(<ImageLoader/>,document.getElementById('messages'));

        event.preventDefault();
        axios.post(global.host + 'article',this.state).then(function (xhr) {
            if(xhr.data) {
                ReactDOM.render(
                    <div className="alert alert-success text-center">The article added correctly</div>
                    ,document.getElementById('messages')
                )
                    ReactDOM.render(<ImageLoader/>,document.getElementById('main'),function () {
                        ReactDOM.render(<AllArticles/>,document.getElementById('main'))
                    })
            }
        }).catch(function (xhr) {
            if(xhr.response) {
                ReactDOM.render(
                    <div className="alert alert-danger">
                        <ul>
                            {
                                xhr.response.data.error.map((value,key)=>
                                <li key={key}>{value}</li>
                                )
                            }
                        </ul>
                    </div>,document.getElementById('messages')
                )
            }
        })
    }
    handleChange(event) {
        var value = event.target.value;
        var field = event.target.name
        switch (field) {
            case 'title':
                this.setState({title: value});
                break;
            case 'body':
                this.setState({body: value});
                break;
        }
    }
    constructor() {
        super()
        this.state = {
            title: null,
            body: null
        }
        this.handleChange = this.handleChange.bind(this)
        this.addArticle = this.addArticle.bind(this)
    }
    render() {
        return (
            <aside>
                <h3>Add new article</h3>
                <hr />
                <form onSubmit={this.addArticle}>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Title</label>
                        <input type="text" className="form-control" id="title" name="title"
                               aria-describedby="emailHelp" placeholder="Enter Title text" required onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Body</label>
                        <textarea className="form-control" required placeholder="article body" name="body" onChange={this.handleChange}></textarea>
                    </div>
                    <button type="submit" className="btn btn-primary">Add</button>
                </form>
                <div id="messages"></div>
            </aside>
        )
    }
}
export default AddArticle